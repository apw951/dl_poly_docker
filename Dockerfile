FROM opensuse/tumbleweed

RUN zypper in -y shadow && groupadd -g 999 drFaustroll && useradd -r -u 999 -g drFaustroll -m drFaustroll  &&  zypper dup -y  && zypper  in -y  git gnu-compilers-hpc gnu-compilers-hpc-devel \
openmpi4-gnu-hpc openmpi4-gnu-hpc-devel cmake tar xz vim zlib-devel libopenblas-gnu-hpc-devel sqlite3 sqlite3-devel python311 python3-devel python3-pip python3-tox python3-flake8 gsl-gnu-hpc-devel && zypper clean 

WORKDIR /home/drFaustroll
USER drFaustroll
COPY assets/*  /home/drFaustroll/
RUN ./newsoftware.sh && rm -f ./newsoftware.sh && rm -f ./modules-* 
