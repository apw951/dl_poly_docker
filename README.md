## Authentication

- ```docker login registry.gitlab.com```

## Building

- ```./build.sh```

## Deploying

- ```docker tag opensuse-dlpoly:full registry.gitlab.com/ccp5/dlpoly-py```
- ```docker push registry.gitlab.com/ccp5/dlpoly-py```

____________________________________________________________________

### Notes on setup

#### Docker credential storing

- **Default uses plaintext!**, use "pass" on linux to store properly

https://docs.docker.com/engine/reference/commandline/login/

This issue helps a lot with setting up a password with pass and docker 
https://github.com/docker/docker-credential-helpers/issues/102

#### Registering a container

- build with ```sudo docker build -t registry.gitlab.com/apw951/NAME_OF_CONTAINER .```
- upload with ``` sudo docker push registry.gitlab.com/apw951/NAME_OF_CONTAINER```

https://docs.gitlab.com/ee/user/packages/container_registry/index.html

#### Use in CI

- add ```image: registry.gitlab.com/apw951/dlpoly-py``` into the CI YAML file
