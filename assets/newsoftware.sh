#!/usr/bin/env bash
set -x
rm -rf $HOME/dlpoly
rm -rf $HOME/modules
source /etc/profile.d/lmod.sh

module load gnu openmpi openblas
git clone https://gitlab.com/ccp5/dl-poly.git dl-poly-git
pushd dl-poly-git 
v="$(git describe --tags --first-parent --abbrev=11 --long --dirty --always)"
popd
mkdir -p $HOME/modules/dl_poly 
mv dl_poly.module $HOME/modules/dl_poly/$v
sed -i "s/5.0.0/$v/g" $HOME/modules/dl_poly/$v
echo "source /etc/profile.d/lmod.sh" >> ~/.bashrc
echo "module use $HOME/modules" >> ~/.bashrc


cmake -S dl-poly-git -Bbuild-dlpoly -DWITH_KIM=Off -DWITH_PLUMED=Off -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$HOME/dl_poly/gcc13/$v                                                                                               
cmake --build build-dlpoly   
cmake --install build-dlpoly
